#!/bin/sh

while true; do 
upower -i `upower -e | grep 'BAT'`| grep -E "state|to\ full|percentage" > ~/.scripts/.batteryinfo

PERCENTAGE=$(grep -o [[:digit:]]*% ~/.scripts/.batteryinfo)

NUMBER="${PERCENTAGE//%}"

CHARGING=$(grep -o charging ~/.scripts/.batteryinfo)

if [ $NUMBER -lt  10 ] 
then 
	
	
	echo $NUMBER
	echo $CHARGING

	if [ $CHARGING -eq "charging" ]
	then break
	fi

	zenity --error --text="You\'re battery is at $NUMBER%.\n  Time to plug it in\!" --title="Warning\!" --width=280
fi
sleep 60
done

